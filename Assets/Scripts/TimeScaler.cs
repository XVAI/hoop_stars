﻿using System.Collections;
using UnityEngine;

public class TimeScaler : MonoBehaviour
{
    #region Private varibles
    [SerializeField]
    private float slowMotionFactctor = 0.25f;
    [SerializeField]
    private float slowMotionLengt = 1;
    private Coroutine lastRoutine = null;
    #endregion

    #region Public varibles
    public delegate void EndSlowMotion();
    public static event EndSlowMotion OnEndSlowMotion;
    #endregion

    #region Methods
    void Start()
    {
        Score.OnMaxScore += StartFrezeTime;
        Score.OnEndTime += StartFrezeTime;
        PoolGameObjects.OnReload += UnFrezeTime;
        UiManager.OnDiamond += UnFrezeTime;
    }

    private void OnDestroy()
    {
        Score.OnMaxScore -= StartFrezeTime;
        Score.OnEndTime -= StartFrezeTime;
        PoolGameObjects.OnReload -= UnFrezeTime;
        UiManager.OnDiamond -= UnFrezeTime;
    }

    public void StartFrezeTime() 
    {
        lastRoutine = StartCoroutine(FrezeTime());
    }

    public IEnumerator FrezeTime()
    {
        while (Time.timeScale !=0 )
        {
            Time.timeScale = slowMotionFactctor;
            Time.fixedDeltaTime = Time.timeScale * 0.02f;
            Time.timeScale = Mathf.Clamp(Time.timeScale, 0f, 1f);
            yield return new WaitForSeconds(slowMotionLengt);
            Time.timeScale = 0f;
        }
        OnEndSlowMotion.Invoke();
    }

    public void UnFrezeTime()
    {
        StopCoroutine(lastRoutine);
        Time.timeScale = 1f;
        Time.fixedDeltaTime = Time.timeScale * 0.02f;
    }
    #endregion
}
