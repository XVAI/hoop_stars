﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public enum Level
{
    classic,
    diamond
}
public class Score : MonoBehaviour
{
    #region Private varibles
    [SerializeField]
    private int timeRound = 60;
    [SerializeField] private int maxScore = 5;
    private Text timerText;
    private int scorePlayer1 = 0;
    private int scorePlayer2 = 0;
    private Text scoreTextPlayer1;
    private Text scoreTextPlayer2;
    private int diamontCount;
    private Text diamontText;
    private GameObject winPanel;
    private Animation animScoreTextPlayer1;
    private Animation animScoreTextPlayer2;
    private Coroutine lastRoutine = null;
    private Level mylevel;
    #endregion

    #region Public varibles
    public delegate void MaxScore();
    public static event MaxScore OnMaxScore;
    public static event MaxScore OnEndTime;
    public static event MaxScore WinPlayer1;
    public static event MaxScore WinPlayer2;
    public static event MaxScore GoClassic;
    #endregion

    #region Methods
    void Start()
    {
        TimeScaler.OnEndSlowMotion += ShowWinerPanel;
        PoolGameObjects.OnReload += Reload;
        Sphere.OnGoalPlayer1 += AddScorePlayer1;
        Sphere.OnGoalPlayer2 += AddScorePlayer2;
        UiManager.OnDiamond += DiamondStage;
        Diamond.OnAddDiamon += AddDiamond;
        Init();
        SetAtiveText(false);
    }

    private void Update()
    {
        scoreTextPlayer1.text = "" + scorePlayer1;
        scoreTextPlayer2.text = "" + scorePlayer2;
    }

    private void OnDestroy()
    {
        TimeScaler.OnEndSlowMotion -= ShowWinerPanel;
        PoolGameObjects.OnReload -= Reload;
        Sphere.OnGoalPlayer1 -= AddScorePlayer1;
        Sphere.OnGoalPlayer2 -= AddScorePlayer2;
        UiManager.OnDiamond -= DiamondStage;
        Diamond.OnAddDiamon -= AddDiamond;
    }

    private void Init()
    {
        scoreTextPlayer1 = gameObject.transform.GetChild(0).gameObject.GetComponent<Text>();
        scoreTextPlayer2 = gameObject.transform.GetChild(1).gameObject.GetComponent<Text>();
        animScoreTextPlayer1 = gameObject.transform.GetChild(0).gameObject.GetComponent<Animation>();
        animScoreTextPlayer2 = gameObject.transform.GetChild(1).gameObject.GetComponent<Animation>();
        timerText = gameObject.transform.GetChild(2).gameObject.GetComponent<Text>();
        diamontText = gameObject.transform.GetChild(3).gameObject.GetComponent<Text>();
        winPanel = GameObject.Find("/Canvas/WinWindow");
        winPanel.SetActive(false);
        timerText.gameObject.SetActive(false);
    }

    public void AddDiamond()
    {
        diamontCount++;
        diamontText.text = "Diamond: " + diamontCount;
    }

    public void AddScorePlayer1()
    {
        scorePlayer1++;
        animScoreTextPlayer1.Play();
        if (scorePlayer1 >= maxScore) 
        {
            OnMaxScore?.Invoke();
        } 
    }

    public void AddScorePlayer2()
    {
        scorePlayer2++;
        animScoreTextPlayer2.Play();
        if (scorePlayer2 >= maxScore)
        {
            OnMaxScore?.Invoke();
        } 
    }

    public void ChekWinner()
    {
        if(scorePlayer1 >= scorePlayer2) WinPlayer1?.Invoke();
        else WinPlayer2?.Invoke();
    }

    public void SetAtiveText( bool active)
    {
        scoreTextPlayer1.gameObject.SetActive(active);
        scoreTextPlayer2.gameObject.SetActive(active);
        diamontText.gameObject.SetActive(false);
    }

    public void ShowWinerPanel()
    {
        if (mylevel == Level.classic) ChekWinner();
        StopCoroutine(lastRoutine);
        winPanel.SetActive(true);
        if(mylevel == Level.diamond)
        {
            GoClassic.Invoke();
            mylevel = Level.classic;
        }
    }

    public void Reload()
    {
        scorePlayer1 = 0;
        scorePlayer2 = 0;
        SetAtiveText(true);
        StopCoroutine(lastRoutine);
        StartTimer();
        winPanel.SetActive(false);
        mylevel = Level.classic;
    }

    public void DiamondStage()
    {
        SetAtiveText(false);
        diamontText.gameObject.SetActive(true);
        StartTimer();
        winPanel.SetActive(false);
        mylevel = Level.diamond;
    }

    public void StartTimer()
    {
        lastRoutine =  StartCoroutine(Timer(timeRound));
        timerText.gameObject.SetActive(true);
    }

    public IEnumerator Timer(int time)
    {
        float secT = time % 60;
        float minT = time / 60;
        var textProd = timerText;
        while (time != 0)
        {
            var Csec = String.Format("{0:00}", secT);
            var Cmin = String.Format("{0:00}", minT);
            textProd.text = Cmin + ":" + Csec;
            if (secT != 0 && secT <= 60)
            {
                secT--;
                time--;
                yield return new WaitForSeconds(1);
            }
            if (secT == 00 && minT != 0)
            {
                minT--;
                secT = 60;
            }
            textProd.text = Cmin + ":" + Csec;
        }
        OnEndTime?.Invoke();
    }
    #endregion
}
