﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Diamond : Sphere
{
    #region Public varibles
    public delegate void Diamon();
    public static event Diamon OnAddDiamon;
    #endregion

    #region Methods
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "PlayerCenterBottom")
        {
            bottom = true;
        }
        else if (other.gameObject.tag == "PlayerCenterUp") up = true;
        if (bottom && up)
        {
            GoNewPoin();
            if (other.gameObject.transform.parent.tag == "Player1") OnAddDiamon?.Invoke();  
        }
    }
    #endregion
}
