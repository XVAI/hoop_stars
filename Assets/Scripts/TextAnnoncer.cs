﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TextAnnoncer : MonoBehaviour
{
    #region Private varibles
    [SerializeField] private string[] phrases;
    [SerializeField] private float timeLifeText=1f;
    private Text textView;
    #endregion

    #region Methods
    void Start()
    {
        textView = gameObject.GetComponent<Text>();
        textView.enabled = false;
        Sphere.OnGoalPlayer1 += GoAnnounce;
        Sphere.OnGoalPlayer2 += GoAnnounce;
    }

    private void OnDestroy()
    {
        Sphere.OnGoalPlayer1 -= GoAnnounce;
        Sphere.OnGoalPlayer2 -= GoAnnounce;
    }

    public void GoAnnounce()
    {
        textView.enabled = true;
        StartCoroutine(AnnounceGoal());
    }

    public IEnumerator AnnounceGoal()
    {
        int i = Random.Range(0, phrases.Length);
        textView.text = "" + phrases[i];
        yield return new WaitForSeconds(timeLifeText);
        textView.text = "";
    }
    #endregion
}
