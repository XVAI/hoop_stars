﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    #region Private varibles
    [SerializeField] private int sceneID=1;
    private Slider slider;
    #endregion

    #region Methods
    void Start()
    {
        slider = this.gameObject.transform.GetChild(0).GetChild(0).GetComponent<Slider>();
        StartCoroutine(AscynLoad());
    }

    IEnumerator AscynLoad()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneID);
        while (!operation.isDone)
        {
            float progress = operation.progress / 0.9f;
            slider.value = progress;
            yield return null;
        }
    }
    #endregion
}
