﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManager : MonoBehaviour
{
    #region Private varibles
    [SerializeField] private int maxStage = 2;
    private Text TextWin;
    private GameObject diamondButton;
    private GameObject nextButton;
    private int stage = 0;
    private Image[] iconsStage;
    #endregion

    #region Public varibles
    public delegate void MaxStage();
    public static event MaxStage OnMaxStage;
    public static event MaxStage OnDiamond;
    #endregion

    #region Methods
    void Start()
    {
        Score.WinPlayer1 += ShowWin;
        Score.WinPlayer2 += ShowLose;
        Score.GoClassic += GoClassicLevel;
        TextWin = this.gameObject.transform.GetChild(1).GetComponent<Text>();
        diamondButton = this.gameObject.transform.GetChild(3).gameObject;
        nextButton = this.gameObject.transform.GetChild(2).gameObject;
        iconsStage = new Image[maxStage];
        LoadIconsStage();
    }

    private void OnDestroy()
    {
        Score.WinPlayer1 -= ShowWin;
        Score.WinPlayer2 -= ShowLose;
        Score.GoClassic -= GoClassicLevel;
    }

    public void GoDiamondLevel()
    {
        OnDiamond.Invoke();
        ShowDiamondButton(false);
    }

    public void GoClassicLevel()
    {
        stage = 0;
        LoadIconsStage();
        ShowDiamondButton(false);
    }

    public void LoadIconsStage()
    {
        for(int i=0; i < maxStage; i++)
        {
            iconsStage[i] = this.gameObject.transform.GetChild(4).GetChild(i).GetComponent<Image>();  
        }
    }

    public void ColidIcons()
    {
        for (int i = 0; i < iconsStage.Length; i++)
        {
           if(i<=stage) iconsStage[i].color = Color.green;
            else iconsStage[i].color = Color.white;
        }    
    }
    
    private void ShowDiamondButton(bool active)
    {
        diamondButton.SetActive(active);
        nextButton.SetActive(!active);
    }

    public void ShowWin()
    {
        TextWin.text = "You Win";
        if (stage > maxStage) ShowDiamondButton(true);
        else 
        {
            ColidIcons();
            ShowDiamondButton(false);
        }
        if (stage == maxStage) 
        {
            ColidIcons();
            OnMaxStage.Invoke();
        } 
        stage++;
    }

    public void ShowLose()
    {
        TextWin.text = "You Lose";
        stage = 0;
        diamondButton.SetActive(false);
    }
    #endregion
}
