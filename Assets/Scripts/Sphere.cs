﻿using System.Collections;
using UnityEngine;
using DG.Tweening;

public class Sphere : MonoBehaviour
{
    #region Private varibles
    [SerializeField] private protected Vector3[] pointsSpawn;
    [SerializeField]
    private Vector3 punch = new Vector3 (0,-0.5f,0);
    [SerializeField]
    private float duration = 0.5f;
    [SerializeField]
    private int vibrato =1;
    [SerializeField]
    private float elasticity=0;
    private Score score;
    private protected bool up =false;
    private protected bool bottom = false;
    private bool goPunch = true;
    private bool goNewPotision = false;
   
    public bool chekPlayer = true;
    #endregion

    #region Public varibles
    public delegate void Goal();
    public static event Goal OnGoalPlayer1;
    public static event Goal OnGoalPlayer2;
    #endregion

    #region Methods
    void Start()
    {
        score = GameObject.Find("/Boundaries/Canvas/").GetComponent<Score>();
        UiManager.OnMaxStage += ApllyMove;
        Score.GoClassic += DenyMove;

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (goPunch) StartCoroutine(Punch());
    }

    private void OnTriggerEnter(Collider other)
    {
       
            if (other.gameObject.tag == "PlayerCenterBottom")
            {
                bottom = true;
            }
            else if (other.gameObject.tag == "PlayerCenterUp") up = true;
            if (bottom && up)
            {
                if (goNewPotision) GoNewPoin();
                if (other.gameObject.transform.parent.tag == "Player1") OnGoalPlayer1?.Invoke();
                else OnGoalPlayer2?.Invoke();
            up = false;
            }
            
        
       
    }

    private void OnTriggerExit(Collider other)
    {
       
        if (other.gameObject.tag == "PlayerCenterBottom")
        {
            bottom = false;
        }
       // else if (other.gameObject.tag == "PlayerCenterUp") up = false;
       
       
    }

    private void ApllyMove()
    {
        goNewPotision = true;
    }

    private void DenyMove()
    {
        goNewPotision = false;
    }

    private protected void GoNewPoin()
    {
        bool gonext = true;
        int i;
        while (gonext)
        {
            i = Random.Range(1, pointsSpawn.Length);
            if (gameObject.transform.position.x != pointsSpawn[i].x) 
            {
                gameObject.transform.position = pointsSpawn[i];
                gonext = false;
            } 
        } 
    }

    IEnumerator Punch()
    {
        goPunch = false;
        Tween myTween = gameObject.transform.DOPunchPosition(punch, duration, vibrato, elasticity, false);
        yield return myTween.WaitForCompletion();
        goPunch = true;

    }

    #endregion
}
