﻿using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    #region Private varibles
    [SerializeField]
    private protected float jumpY = 5f;
    [SerializeField]
    private protected float jumpX = 2f;
    [SerializeField]
    private protected float powerLeft = 50f;
    [SerializeField]
    private protected float powerUp = 75f;
    [SerializeField]
    private protected float maxSpeed = 10;
    [SerializeField]
    private protected float gravity = -10f;
    private protected Rigidbody _rb;
    private protected Text playerText;
    private string playerName;
    private Button leftForceButton;
    private Button rigthForceButton;
    #endregion

    #region Methods
    void Start()
    {
        _rb = GetComponent<Rigidbody>();
        playerText = this.gameObject.transform.GetChild(0).GetChild(0).GetComponent<Text>();
        leftForceButton = GameObject.Find("/Canvas/LeftForce/").GetComponent<Button>();
        rigthForceButton = GameObject.Find("/Canvas/RightForce/").GetComponent<Button>();
        leftForceButton.onClick.AddListener(LeftForce);
        playerText.text = playerName;
        rigthForceButton.onClick.AddListener(RigthForce);
    }
    
    void Update()
    {
        Physics.gravity = new Vector3(0, gravity, 0);
    }

    private void FixedUpdate()
    {
        MaxSpeed();
    }

    public void SetName(string name)
    {
        playerName = name;
    }

    public void MaxSpeed()
    {
        if (_rb.velocity.magnitude > maxSpeed)
        {
            _rb.velocity = _rb.velocity.normalized * maxSpeed;
        }
    }

    public void LeftForce()
    {
        if (_rb.velocity.x > 0) _rb.velocity = new Vector3(-_rb.velocity.x, _rb.velocity.y, _rb.velocity.z);
        _rb.AddForce(new Vector3(-jumpX * powerLeft, jumpY * powerUp, 0));
    }

    public void RigthForce()
    {
        if (_rb.velocity.x < 0) _rb.velocity = new Vector3(-_rb.velocity.x, _rb.velocity.y, _rb.velocity.z);
        _rb.AddForce(new Vector3(jumpX * powerLeft, jumpY * powerUp, 0));
    }
    #endregion
}
