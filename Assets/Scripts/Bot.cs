﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Bot : Player
{
    #region Private varibles  
    [SerializeField] private string[] names;
    [SerializeField]
    [Range(1f,10f)]
    private float dificult=5;
    [SerializeField]
    private float frequency=1;
    private Coroutine lastRoutine = null;
    private Transform transformShepre;
    private Level myLevel;
    #endregion

    #region Methods
    void Start()
    {
        Score.OnMaxScore += StopMove;
        Score.OnEndTime += StopMove;
        Score.GoClassic += GoClassicLevel;
        PoolGameObjects.OnReload += StartMove;
        UiManager.OnDiamond += OnDiamondLevel;
        myLevel = Level.classic;
        playerText = this.gameObject.transform.GetChild(0).GetChild(0).GetComponent<Text>();
        transformShepre = GameObject.Find("/Sphere").GetComponent<Transform>();
        _rb = GetComponent<Rigidbody>();
        StartMove();
    }
    
    private void OnDestroy()
    {
        Score.OnMaxScore -= StopMove;
        Score.OnEndTime -= StopMove;
        PoolGameObjects.OnReload -= StartMove;
        UiManager.OnDiamond -= StopMove;
        Score.GoClassic -= GoClassicLevel;
    }

    public void SetName()
    {
        int i = Random.Range(0, names.Length);
        playerText.text =""+ names[i];
    }

    private void OnDiamondLevel()
    {
        myLevel = Level.diamond;
        StopMove();
    }

    private void GoClassicLevel()
    {
        myLevel = Level.classic;
    }

    public  void StartMove()
    {
       if( myLevel == Level.classic)
        {
            SetName();
            lastRoutine = StartCoroutine(Move());
        }
    }

    public void StopMove()
    {
        StopCoroutine(lastRoutine);
    }

    public void MoveToSphere()
    {
        if (transformShepre.position.x - transform.position.x < 0) LeftForce();
        else RigthForce();
    }

    public void GoRandom ()
    {
        int i = Random.Range(0, 1);
        if(i==0) LeftForce();
        else RigthForce();
    }

    public IEnumerator Move()
    {
       bool goMove = true;
        while (goMove)
        {
            int i = Random.Range(1,10);
            if (dificult >= i) MoveToSphere();
            else GoRandom();
            yield return new WaitForSeconds(frequency);
        }   
    }
    #endregion
}
