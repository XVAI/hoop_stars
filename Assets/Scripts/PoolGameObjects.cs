﻿using UnityEngine;

public class PoolGameObjects : MonoBehaviour
{
    #region Private varibles
    [SerializeField] private PoolObject[] pool;
    [SerializeField] private GameObject[] poolGameObject;
    [SerializeField] private string namePlayer;
    #endregion

    #region Public varibles
    public delegate void Reload();
    public static event Reload OnReload;
    #endregion

    #region Methods
    void Start()
    {
        poolGameObject = new GameObject[pool.Length];
        Init();
        UiManager.OnDiamond += DiamondStage;
        Score.GoClassic += ClassicStage;
    }

    private void OnDestroy()
    {
        UiManager.OnDiamond -= DiamondStage;
        Score.GoClassic -= ClassicStage;
    }

    public void DiamondStage()
    {
        poolGameObject[3].SetActive(true);
        poolGameObject[2].SetActive(false);
        poolGameObject[0].SetActive(false);
        GoStartTransform();
    }

    public void ClassicStage()
    {
        poolGameObject[3].SetActive(false);
    }

    public void SaveNamePlayer(string text)
    {
        if (!string.IsNullOrEmpty(text))
        {
            namePlayer = text;
        }  
    }

    public void SetNamePlayer(int i)
    {
        if (poolGameObject[i].GetComponent<Player>() != null)
        {
            poolGameObject[i].GetComponent<Player>().SetName(namePlayer);
        }
    }

    public void GoReload()
    {
        OnReload.Invoke();
        GoStartTransform();
    }

    public void GoStartTransform()
    {
        for (int i = 0; i < poolGameObject.Length; i++)
        {
            poolGameObject[i].transform.position = pool[i].startPosition;
            poolGameObject[i].transform.rotation = pool[i].startRotation;
        }
    }

    public void Init()
    {
        for (int i = 0; i < pool.Length; i++)
        {
            poolGameObject[i] = Instantiate(pool[i].gameObject, pool[i].startPosition, pool[i].startRotation);
            if (pool[i].name == "") pool[i].name = pool[i].gameObject.name;
            poolGameObject[i].name = pool[i].name;
            poolGameObject[i].SetActive(false);
        }  
    }

    public void OnActive()
    {
        for (int i = 0; i < pool.Length-1; i++)
        {
            poolGameObject[i].SetActive(true);
            SetNamePlayer(i);
        }
    }
    #endregion
}

[System.Serializable]
public struct PoolObject
{
    public string name; 
    public GameObject gameObject;
    public Vector3 startPosition;
    public Quaternion startRotation;
}
