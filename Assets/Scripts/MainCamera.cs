﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MainCamera : MonoBehaviour
{
    #region Private varibles
    [SerializeField]
    private float durationShake=1f;
    [SerializeField]
    private float strengthShake=0.5f;
    [SerializeField]
    private int vibratoShake=5;
    [SerializeField]
    private float randomessShake=45;
    [SerializeField]
    private bool fadeOutShake=true;
    #endregion

    #region Methods
    void Start()
    {
        Sphere.OnGoalPlayer1 += ShakeCamera;
        Sphere.OnGoalPlayer2 += ShakeCamera;
    }

    private void OnDestroy()
    {
        Sphere.OnGoalPlayer1 -= ShakeCamera;
        Sphere.OnGoalPlayer2 -= ShakeCamera;
    }

    private  void ShakeCamera()
    {
        Camera.main.DOShakePosition(durationShake,strengthShake, vibratoShake,randomessShake,fadeOutShake);
    }
    #endregion
}
